# Introduction to Computer Science
Laramie High School  
Laramie, WY  
2018-2019  
 
## About this curriculum

This is a modified version of the curriculum provided by the TEALS program. It contains both Semester 1 and Semester 2 material for the Introduction to Computer Science course and is customized for the needs of the Laramie High School 2018-2019 TEALS teaching team.

The original material can be found via the links below:
- Semester 1: https://github.com/TEALSK12/introduction-to-computer-science
- Semester 2: https://github.com/TEALSK12/2nd-semester-introduction-to-computer-science

### License
This curriculum is licensed under the Creative Commons Attribution Non-Commercial Share-alike License (http://creativecommons.org/licenses/by-nc-sa/4.0/), which means you may share and adapt this material for non-commercial uses as long as you attribute its original source, and retain these same licensing terms.

## Online GitBook
https://lhscompsci.gitlab.io/2018curriculum

## Downloads
[HTML](https://gitlab.com/lhscompsci/2018curriculum/-/jobs/artifacts/master/download?job=html)  
[PDF](https://gitlab.com/lhscompsci/2018curriculum/-/jobs/artifacts/master/download?job=pdf)  

## Source
https://gitlab.com/lhscompsci/2018curriculum/